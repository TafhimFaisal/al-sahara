<?php include './config.php'?>
<?php include './views/layouts/__head.php'?>
<?php include './views/layouts/header.php'?>
<?php include './views/layouts/navbar.php'?>
<div class="page-banner">
    <img src="http://<?=$_SERVER['HTTP_HOST']?>/images/about-us-banner.jpg" alt="" srcset="">
</div>
<?php include './views/layouts/bannerup.php'?>

<div class="content container text-justify" style="padding:0px 240px">

  
  <h1> ABOUT US </h1>
  We are one of the reputed manpower supply companies in UAE headquarter in Ajman,
  focused on providing manpower supply services throughout the UAE for the past 10 years. 
  We are the one-stop solution for manpower supply in UAE to the  requirement of the general 
  and technical services for all major industrial sectors such as Civil Construction, 
  Mechanical, Electrical, Hospitality & Logistics. 
  With years of service, we at AL SAHARA in UAE have assisted more than Twenty 
  organizations of the different industrial sectors in UAE with skilled and unskilled labors. 
  Our goal is to provide quality manpower supply services to all our UAE clients. 
      <ul>
        <li>We are the right Labor Supply Partner to meet your business goal.</li>
        <li>Ensure your success with our expertise in Manpower Supply Services.</li>
      </ul>
  REASON TO CHOOSE US AS YOUR MANPOWER SUPPLY COMPANY
  All businesses require talented manpower resources to execute the projects on time. But it 
  is a costly and time-consuming affair to hire such skilled and talented manpower for companies. 
  Here comes the role of Manpower Supply Company that provides quick and easy access to the best 
  workers to companies of different sectors. We offer a swift and easy transition for the companies 
  who are in need of qualified manpower. 
  We are one of the most prominent manpower supply companies in UAE. We offer premium quality labor 
  supply services in all the emirates of the UAE. We at Manpower Supply provide skilled and unskilled 
  manpower to different companies of various sectors for the Long-Term as well as Short-Term.
  


</div>


<?php include './views/layouts/bannerupdown.php'?>
<?php include './views/layouts/footer.php'?>
<?php include './views/layouts/__foot.php'?>