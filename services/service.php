<?php include '../config.php'?>
<?php include '../views/layouts/__head.php'?>
<?php include '../views/layouts/header.php'?>
<?php include '../views/layouts/navbar.php'?>
<div class="page-banner">
    <img src="../images/Our-Services-Banner.jpg" alt="" srcset="">
</div>
<?php include '../views/layouts/bannerup.php'?>

<div class="container" style="padding:0px 240px">
    <div class="text-justify">
        There are different kinds of skills to execute a wide range of   industries. Each of the jobs needs special skills and experience to complete the job professionally. We do provide manpower supply services to the following industries in UAE:
            <ul>
                <li> Civil - Construction Industry </li>
                <li> Mechanical, Electrical and Plumbing – (MEP) </li>
                <li> Hospitality (Catering / Cleaning) </li>
            </ul>
        The Manpower we provide is skilled, Trained and certified according to the Industry Standards. They are committed to the health and safety and execute the work without any chance of accidents. It is hard to get the skilled workforce but we can provide you with the pool of skilled and talented workforce at affordable rates. 
        We have the experience to provide a different kind of  manpower to different companies. So whatever sector your company belongs to, then we can provide you with the best and skilled workforce in a short time in a timely manner.
    </div>

   
</div>

<?php include '../views/layouts/bannerupdown.php'?>
<?php include '../views/layouts/footer.php'?>
<?php include '../views/layouts/__foot.php'?>