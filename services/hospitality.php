<?php include '../config.php'?>
<?php include '../views/layouts/__head.php'?>
<?php include '../views/layouts/header.php'?>
<?php include '../views/layouts/navbar.php'?>
<div class="page-banner">
    <img src="../images/hospitality-uniforms-banner.jpg" alt="" srcset="">
</div>
<?php include '../views/layouts/bannerup.php'?>

<div class="container">
    
    <h1 class="text-center">
        HOSPITALITY INDUSTRY MANPOWER SUPPLY
    </h1> 

    <p class="text-justify">
        We supply Waiter / Waitress / Housekeeping Worker at very reasonable cost for Hotels, Restaurants and other establishments. We have full trained Waiters / Waitresses. The Hospitality Industry Manpower Supply includes - 
    </p>
    <ul>
    <li> For Catering 
        <ul>
        <li>Cook / Assistant Cook</li>
        <li>Kitchen Helper</li>
        <li>Dish Washers</li>
        </ul>
    </li>

    <li> For Hotel
        <ul>
        <li>Waiters / Waitresses</li>
        <li>House Keeper</li>
        </ul>   
    </li>
    </ul>

    <h2> FACTORY / PACKING / HELPER MANPOWER SUPPLY</h2>
    <p class="text-justify">
        As the name suggests Factory / Packing Manpower terms are having several years of experience they can complete the work ahead of the deadline, our Factory / Packing  Helper Manpower Supply includes – 
    </p>  
    <ul>
    <li>Containers Loading - Unloading Helper</li>
    <li>Machine Operator</li>
    <li>Packing Helper</li>
    </ul>

    <h2> CLEANING HELPER MANPOWER SUPPLY</h2>

    <p class="text-justify">
        As the name suggests Cleaner Manpower terms are having several years of experience they can complete  the work ahead of the deadline, our Cleaning Manpower Supply includes – 
    </p>
    
    <ul>
        <li>Road Cleaner
            <ul>
                <li>Resident Cleaner</li>
                <li>Hospital Cleaner</li>
            </ul>  
        </li>
        <li>Market Cleaner       
            <ul> 
                <li>Construction Cleaner</li> 
                <li>Car Wash</li>
            </ul>
        </li>
        <li>Watchman</li>
    </ul>
</div>


<?php include '../views/layouts/bannerupdown.php'?>
<?php include '../views/layouts/footer.php'?>
<?php include '../views/layouts/__foot.php'?>