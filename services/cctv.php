<?php include '../config.php'?>
<?php include '../views/layouts/__head.php'?>
<?php include '../views/layouts/header.php'?>
<?php include '../views/layouts/navbar.php'?>
<div class="page-banner">
    <img src="../images/cctv-banner.jpg" alt="" srcset="">
</div>
<?php include '../views/layouts/bannerup.php'?>
<div class="container">
    <h1 class='text-center'> CCTV Services </h1> 
    <p class='text-justify'> 
        We are committed to give the customers a seamless experience in installation,                 configuration, maintenance and upkeep, upgradation, repairs and more for their electronic security, surveillance equipment and robust cabling network. 
    </p>


    <h2> Supply & Installation </h2>
    <p class='text-justify'> 
        As a professional security and surveillance system providers, we have the best in class systems and cabling network for every need and every budget. We offer complete handover and training on installation of the systems and network.
    </p>

    <h2> Maintenance Contracts </h2>
    <p class='text-justify'> 
        We offer comprehensive maintenance contracts for our clients that include preventive maintenance,     regular repairs, system expansion, upgrades and more of both your hardware and software systems. Since we are conveniently located in the heart of UAE, our team will reach you in record time so that you enjoy minimal downtime.
    </p>

    <h2> Project Management & Consultancy </h2>
    <p class='text-justify'> 
        We offer 360° security solutions using proactive approach to give our clients total security from both manmade and natural hazards. Our experts will design security and network solutions according to the nature, size and unique requirements of your business.
    </p>


    <h2> Our Product Categories:</h2>
    <ol>
        <li>CCTV & SURVEILLANCE</li>
        <li>ACCESS CONTROL & TIME ATTENDANCE</li>
        <li>IP TELEPHONY & PABX</li>
        <li>AUDIO / VIDEO INTERCOM</li>
        <li>STRUCTURED CABLING</li>
    </ol>

    <h2> CCTV & SURVEILLANCE</h2>
    <p class='text-justify'> 
        AL SAHARA is a leading service provider for ALL types of CCTV and Surveillance systems. We have entire range of products and brands compatible for use in crime prevention, traffic monitoring, retail outlets,    industrial processes, corporate houses and home security. Our cutting edge surveillance and security systems come with remote viewing to curb the chances of theft, burglary and keep a tab on day to day activities of your employees. We offer end to end service on CCTV and surveillance equipment, right from choosing the right equipment to suit the size and nature of   business to installation, configuration and maintenance of the systems.
    </p>
</div>



<?php include '../views/layouts/bannerupdown.php'?>
<?php include '../views/layouts/footer.php'?>
<?php include '../views/layouts/__foot.php'?>