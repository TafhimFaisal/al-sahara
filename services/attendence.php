<?php include '../config.php'?>
<?php include '../views/layouts/__head.php'?>
<?php include '../views/layouts/header.php'?>
<?php include '../views/layouts/navbar.php'?>
<div class="page-banner">
    <img src="../images/access-control-banner.jpg" alt="" srcset="">
</div>
<?php include '../views/layouts/bannerup.php'?>
<div class="container">
    <h1 class="text-center">  Access Control </h1> 
    <p class="text-justify">
        AL SAHARA offers comprehensive access control solutions including software and hardware solutions. Based on our client’s needs, we offer installation, service, upkeep and maintenance services. Our hospitality systems are widely acclaimed by renowned hotels across the UAE.    
    </p>
    <h2>Time Attendance</h2> 
    <p class="text-justify">
        Keeping a tab on the employee attendance becomes a breeze with feature rich time attendance systems. Record time sheets, calculate working hours and integrate it with Access Control system for easy management. Our access control and time attendance go a long way in enhancing employee efficiency and productivity
    
    </p> 

    <h2> IP TELEPHONY & PABX </h2>
    <p class="text-justify">
        Our clients enjoy viable and scalable VoIP communications through IP network at reduced costs, easy interface of existing data structure and seamless communication and better collaboration with branch offices and customers. Our PABX and IP telephony solutions include IP-PBXs, VoIP ATAs, VoIP Gate always and IP phones.
    </p>
    <p class="text-justify">
        We offer end to end service including installation and configuration of the IP telephony systems as well as upgrades and maintenance services to give our clients a hassle free experience!
    </p>

    <h2> AUDIO/VIDEO INTERCOM </h2>
    <p class="text-justify">
        We design effective, fully functional solutions for commercial, residential and corporate buildings. Be it simple office intercom system to integrated solutions for a large commercial premise, we do it all. Our security solutions encompass the needs of every client. Right from securing the entrances with crisp video and clear audio systems to ensuring effective communication throughout the office premises, warehouse and parking areas, to securing board rooms and top executive offices, our robust audio video intercom solutions are simply unparalleled in terms of quality, service and technical excellence.
    </p>
    <h2>
        STRUCTURED CABLING
    </h2>
    <p class="text-justify">
        We offer integrated structured cabling solutions for robust, uninterrupted and undisrupted communication system. Our experienced and expert team ensures that the complex issues driving the networking systems are tackled diligently at the planning stage so that our clients enjoy a healthy, flawless telecommunication infrastructure. We are committed to offer strong, scalable cabling infrastructure to meet the current and future needs of our clients. Providing complete structured cabling solutions, every step right from designing, planning, installation and maintenance of your network systems is well taken care of.
    </p>
</div>


<?php include '../views/layouts/bannerupdown.php'?>
<?php include '../views/layouts/footer.php'?>
<?php include '../views/layouts/__foot.php'?>