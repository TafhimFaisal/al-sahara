<?php include '../config.php'?>
<?php include '../views/layouts/__head.php'?>
<?php include '../views/layouts/header.php'?>
<?php include '../views/layouts/navbar.php'?>
<div class="page-banner">
    <img src="../images/ourservices-banner-1.png" alt="" srcset="">
</div>
<?php include '../views/layouts/bannerup.php'?>
<div class="container">

    <h1 class="text-center">
        Mechanical, Electrical and Plumbing – (MEP)
    </h1>
    <p class="text-justify">
        The Manpower we provide is skilled, Trained and certified according to the Industry Standards. They are committed to the health and safety and execute the work without any chance of accidents. It is hard to get the skilled workforce but we can provide you with the pool of skilled and talented workforce at affordable rates. 
    </p>
    <p class="text-justify">
        We have the experience to provide a different kind of  manpower to different companies. So whatever sector your company belongs to, then we can provide you with the best and skilled workforce in a short time in a timely manner.
    </p>
    <h2>
        CONSTRUCTION – MANPOWER SUPPLY SERVICES
    </h2>
    <p class="text-justify">
        We offer different types of “Construction Labor Supply Services” for the Short-Term and Long-Term. Types of manpower supply which we offer for Civil Constructions are:
    </p>
    <ul>
        <li>Mason</li>
        <li>Steel Fixer</li>
        <li>Carpenter</li>
        <li>Scaffold</li>
        <li>Riggers</li>
        <li>Painter</li>
    </ul>
    <h2>
        MECHANICAL –MANPOWER SUPPLY SERVICES
    </h2>
    <p class="text-justify">
        As our “Mechanical Manpower Supply” terms are having several years of experience they can complete the work ahead of the deadline. Our        mechanical Manpower Supply includes:
    </p>
    <ul>
        <li>Welder</li>
        <li>Fabricator</li>
        <li>Pipe Fitter</li>
        <li>Plumber</li>
        <li>Duct Men</li>
        <li>Insulator</li>
    </ul>
</div>



<?php include '../views/layouts/bannerupdown.php'?>
<?php include '../views/layouts/footer.php'?>
<?php include '../views/layouts/__foot.php'?>