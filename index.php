<?php include './config.php'?>
<?php include './views/layouts/__head.php'?>
<?php include './views/layouts/header.php'?>
<?php include './views/layouts/navbar.php'?>
<?php include './views/layouts/slider.php'?>

<div class="section-1">
    
    <div class="section-1-contnet">
        <h1>
            Our Manpower Supply Service Specialties
        </h1>
        <p>
            <ul class="list">
                <li>We are the right Labor Supply Partner to meet your business goal.  </li>
                <li>Ensure your success with our expertise in Manpower Supply Services.  </li>
                <li> We are the ultimate destination for your manpower supply requirements in UAE. </li>
            </ul>

        </p>
    </div>
</div>

<div class="section-2">
    <div class="row boxes">
        <div class="col-4">
            <div class="box">
                <div class="logo"><img src="http://<?=$_SERVER['HTTP_HOST']?>/images/logos/icon_1.png" alt="" srcset=""></div>
                <h1>Reliable Manpower Supply</h1>
                <p>We assure you to provide the best manpower supply services for your business and will never let you down.</p>
            </div>
        </div>
        <div class="col-4">
            <div class="box">
                <div class="logo"><img src="http://<?=$_SERVER['HTTP_HOST']?>/images/logos/icon_5.png" alt="" srcset=""></div>
                <h1>On Time Service</h1>
                <p>We supply the right manpower as per our clients requirements in a timely manner.</p>
            </div>
        </div>
        <div class="col-4">
            <div class="box">
                <div class="logo"><img src="http://<?=$_SERVER['HTTP_HOST']?>/images/logos/icon_6.png" alt="" srcset=""></div>
                <h1>Negotiable Rates</h1>
                <p>Provides skilled manpower supply at negotiable hourly or monthly rates which is the best in industry.</p>
            </div>
        </div>
        <div class="col-4">
            <div class="box">
                <div class="logo"><img src="http://<?=$_SERVER['HTTP_HOST']?>/images/logos/icon_2.png" alt="" srcset=""></div>
                <h1>Flexible Contracts</h1>
                <p>We provide a team or individuals who can meet your manpower requirements perfectly in short term and long term basis.</p>
            </div>
        </div>
        <div class="col-4">
            <div class="box">
                <div class="logo"><img src="http://<?=$_SERVER['HTTP_HOST']?>/images/logos/icon_3.png" alt="" srcset=""></div>
                <h1>Industry Expertise</h1>
                <p>With more than 15 years of expertise in manpower supply services in UAE , we understand the manpower requirements of the clients in a better way.</p>
            </div>
        </div>
        <div class="col-4">
            <div class="box">
                <div class="logo"><img src="http://<?=$_SERVER['HTTP_HOST']?>/images/logos/icon_4.png" alt="" srcset=""></div>
                <h1>Health and Safety</h1>
                <p>Our personnel are trained to the industry standard and there by contribute to a zero harm work place.</p>
            </div>
        </div>
    </div>    

</div>

<div class="section-3 container-fluid">
    <h1 class="text-center"> Our Services </h1>
    <div class="row ">
        <div class="col-4">
            <div class="service-image-1">
                <img src="/images/services/ser1.jpeg" alt="" srcset="">
            </div>
            <div class="service-content">
                <h4>Hospitality (Catering / Cleaning)</h4>
                <p>Highly specialized in providing civil construction manpower supply in UAE.</p>
            </div>
        </div>
        <div class="col-4">
            <div class="service-image-2"><img src="http://<?=$_SERVER['HTTP_HOST']?>/images/services/ser2.jpg" alt="" srcset=""></div>
            <div class="service-content">
                <h4>Civil Construction Industry</h4>
                <p>One of the top manpower supply company in UAE to provide high-standard manpower to Mechanical Industry.</p>
            </div>
        </div>
        <div class="col-4">
            <div class="service-image-3"><img src="http://<?=$_SERVER['HTTP_HOST']?>/images/services/ser3.png" alt="" srcset=""></div>
            <div class="service-content">
                <h4>Mechanical, Electrical and Plumbing(MEP)</h4>
                <p>Providing qualified electrical professionals for any type of commercial works for short & long term basis.</p>
            </div>
        </div>
    </div>
</div>

<div class="section-4 ">

    <div class="section-4-content"> 
        <div class="border-right">426</div>
        <div class="border-left">Projects Completed</div>
    </div>
    <div class="section-4-content"> 
        <div class="border-right">15</div>
        <div class="border-left">Years of Experience</div>
    </div>
    <div class="section-4-content"> 
        <div class="border-right">3</div>
        <div class="border-left">Office in UAE</div>
    </div>
    <div class="section-4-content"> 
        <div class="border-right">5280</div>
        <div class="border-left">Manpower</div>
    </div>

</div>

<div class="section-5 row">
    <div class="col-6"></div>
    <div class="col-6 section-5-content">
        <h2>
            Manpower Supply
            Company
        </h2>
        <p>
            Our mission is to maintain highest level of professionalism, integrity and fairness in our relationship with our customers, suppliers, sub-contractors and professional associates.
        </p>
    </div>
</div>

<div class="section-6">
    <div class="section-6-content">
        <h2>Wanna Talk To Us?</h2>
        <p>Please feel free to contact us for all kind of manpower supply requirements.</p>
    </div>
    <button type="button" class="section-6-button"> Contact </button>
</div>


<?php include './views/layouts/footer.php'?>
<?php include './views/layouts/__foot.php'?>

