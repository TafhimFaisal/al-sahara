
<nav class="navbar navbar-expand-lg text-white custom-style-navbar">

    
  <a class="navbar-brand text-white" href="#">Al Sahara</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse text-white" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto text-white">
      <li class="nav-item active text-white">
        <a class="nav-link text-white" href="/">Home </a>
      </li>
      <li class="nav-item have-submanu">
        <a class="nav-link text-white submanu-head" href="http://<?=$_SERVER['HTTP_HOST']?>/services/service.php">Our Service</a>
        <ul class="sub-manue">
          <li class="sub-manue-item"> <a href="http://<?=$_SERVER['HTTP_HOST']?>/services/civil-construction.php"> MEP </a> </li>
          <li class="sub-manue-item"> <a href="http://<?=$_SERVER['HTTP_HOST']?>/services/hospitality.php"> Hospitality </a> </li>
          <li class="sub-manue-item"> <a href="http://<?=$_SERVER['HTTP_HOST']?>/services/cctv.php"> CCTV </a></li>
          <li class="sub-manue-item"> <a href="http://<?=$_SERVER['HTTP_HOST']?>/services/attendence.php"> Access Controll </a></li>
        </ul>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="http://<?=$_SERVER['HTTP_HOST']?>/aboutus.php">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="http://<?=$_SERVER['HTTP_HOST']?>/contactus.php">Contact Us</a>
      </li>
    </ul>
    
  </div>

</nav>