<div class="header container-fluid">
    <div class="row social-midia-icons">
        <div class="col-10"></div>
        <div class="col-2">
            <i class="fa fa-facebook-official facebook" aria-hidden="true"></i>
            <i class="fa fa-twitter-square twitter" aria-hidden="true"></i>
            <i class="fa fa-linkedin-square linkedin" aria-hidden="true"></i>
            <i class="fa fa-instagram instagram" aria-hidden="true"></i>
        </div>
    </div>
    <div class="row information">
        
        <div class="col-2">
            <div class="brand-icon d-flex justify-content-end">
                <img src="http://<?=$_SERVER['HTTP_HOST']?>/images/logos/logo-final.jpeg" alt="" srcset="">
            </div> 
            <p class="brand-name"> Al-Sahara </p>
        </div>
        <div class="col-10 d-flex align-items-center justify-content-end contact-information">
            <div class="phone d-flex justify-content-center align-items-center">
                <div class="float-left"><i class="fa fa-phone" aria-hidden="true"></i></div>
                +97165322336
            </div>
        
        
            <div class="email d-flex justify-content-center align-items-center">
                <div class="float-left"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                    info.dpellc@gmail.com
            </div>
            <button type="button" class="btn btn-outline-info contact-us-button">Contact Us</button>
        </div>
    </div>
</div>