<footer>
    <div class="container-fluid">
    
        <div class="row footer-info">
            
            <div class="footer-section-1 col-3">
                <div class="footer-logo">
                    <img src="http://<?=$_SERVER['HTTP_HOST']?>/images/logos/logo-final.jpeg" alt="" srcset="">
                </div>
                <p>
                    Greetings from Al Sahara Group of Companies.
                    Sincere gratitude and thanks to all our valuable.
                    <a href="#">Read more >></a><br/>
                    <a href="#">Privacy Policy</a>
                </p>
            </div>

            <div class="footer-section-2 col-3">
                <h3>Manpower Supply Services</h3>
                <ul>
                    <li><a href="http://<?=$_SERVER['HTTP_HOST']?>/services/civil-construction.php">Civil</a></li>
                    <li><a href="http://<?=$_SERVER['HTTP_HOST']?>/services/cctv.php">CCTV</a></li>
                    <li><a href="http://<?=$_SERVER['HTTP_HOST']?>/services/hospitality.php">Hospitality</a></li>
                    <li><a href="http://<?=$_SERVER['HTTP_HOST']?>/services/attendence.php">Access Control</a></li>
                </ul>
            </div>

            <div class="footer-section-3 col-3">
                <h3>Areas of Expertise</h3>
                <ul>
                    <li><a href="http://<?=$_SERVER['HTTP_HOST']?>/services/civil-construction.php">Civil</a></li>
                    <li><a href="http://<?=$_SERVER['HTTP_HOST']?>/services/cctv.php">CCTV</a></li>
                    <li><a href="http://<?=$_SERVER['HTTP_HOST']?>/services/hospitality.php">Hospitality</a></li>
                    <li><a href="http://<?=$_SERVER['HTTP_HOST']?>/services/attendence.php">Access Control</a></li>
                </ul>
            </div>

            <div class="footer-section-4 col-3">
                <h3>Contact Us</h3>
                <p>
                    Mob:    <br>+971 6 53 22 336<br>
                    email:  <br>info.dpellc@gmail.com
                    www.sahara-uae.com
                </p>
            </div>

            <div class="footer-main col-12">
                © 2020, All Rights Reserved, Al Sahara | Designed by Tafhim
            </div>
        </div>
        
    </div>

    
</footer>
