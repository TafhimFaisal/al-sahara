
$(document).ready(function () {
    
    let sliderImageSorce = [
        ['/images/Slider-Final.jpg','dontShow'],
        ['/images/Slider-Final-1-1.jpg','dontShow'],
        ['/images/Slider-2-Final-1.jpg','dontShow'],
    ]

    let prasentImage = 0;

    next = (imageNumber) => {
        
        prasentImage = prasentImage + imageNumber;
        
        if(prasentImage == sliderImageSorce.length ){
            prasentImage = 0;
            showImage(prasentImage)
        }else if( prasentImage < 0 ){
            prasentImage = sliderImageSorce.length - 1;
            showImage(prasentImage)
        }else{
            showImage(prasentImage)
        }
        
        
    }

    showImage = (index) => {
        let img = ''; 
        sliderImageSorce[index][1] = 'show';

        for(i = 0;i<sliderImageSorce.length;i++){
            
                let previousIndex = (index-1) ;

                if(i == previousIndex){
                    sliderImageSorce[i][1] = 'dontshow'
                }else if(i!=index ){
                    sliderImageSorce[i][1] = 'off'
                }
            }

            sliderImageSorce.map((sorce,index) => { img += `<img src="${sorce[0]}" class="${sorce[1]}"  class='slider-image image-${index}' style="" alt="" srcset="">`; })
            $('.slider-images').html(img)
    }

    showImage(prasentImage)

    
    

    $('.previous-button').click( function () { next(-1) })
    $('.next-button').click(function () { next(1) })
});